﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Type
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public string Fermentation { get; set; }
        public string Commentaire { get; set; }
    }
}
