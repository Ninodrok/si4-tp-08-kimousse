﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Pays
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public double Consommation { get; set; }
        public double Production { get; set; }
    }
}
